//
//  main.m
//  BBDemo
//
//  Created by Naveen Sharma on 30/03/16.
//  Copyright © 2016 Naveen Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
