//
//  AppDelegate.h
//  BBDemo
//
//  Created by Naveen Sharma on 30/03/16.
//  Copyright © 2016 Naveen Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

